console.log("Hola MyBalance!");
var express = require ('express');
var app = express();
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var baseMLabURL = "https://api.mlab.com/api/1/databases/mybalancecsm/collections/";
var mLabAPIKey = "apiKey=N6UYqkKSL_kTewMXeFoqRlOeV1yshEkO";
var requestJson = require('request-json'); //para construir el cliente http

app.listen(port);
console.log("MyBalance escuchando en "+port);

app.get("/mybalance",
  function(req,res){
    console.log("GET /mybalance");
    res.send({"msg":"Hola desde MyBalance"});
  }
);

//Obtención del listado de la colección user
app.get("/mybalance/users",
    function(req,res){
      console.log("GET /mybalance/users");
      var httpClient =requestJson.createClient(baseMLabURL);
      console.log("Cliente http creado para obtención de listado de usuarios");
      httpClient.get("user?" + mLabAPIKey,
        function(err, resMLab, body){ //si la api de mlab 4xx hay error en cliente. si 5xx error de servidor. resMLab->respuesta que nos devuleve la petición get con cabeceras. De esa respuesta nos interesa sólo el body que además estará en formato JSON
          var response = !err ? body :{ // si no hay error, devuelve el body. la variable err se devuelve cuando hay error.
            "msg" : "Error obteniendo usuarios"
          }
          res.send(response);
        }
      )
    }
);

//Búsqueda de un usuario concreto
app.get("/mybalance/users/:id",
    function(req,res){
      console.log("GET /mybalance/users/:id");
      var id = req.params.id;
      var query = 'q={"id":' + id + '}';

      var httpClient =requestJson.createClient(baseMLabURL);

      console.log("Cliente http creado para búsqueda de usuario");

      httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body){ //si la api de mlab 4xx hay error en cliente. si 5xx error de servidor. resMLab->respuesta que nos devuleve la petición get con cabeceras. De esa respuesta nos interesa sólo el body que además estará en formato JSON
          //var response = !err ? body :{ // si no hay error, devuelve el body. la variable err se devuelve cuando hay error.
          //  "msg" : "Error obteniendo usuarios"
          var response = {};
          //console.log(body);

          if (err){
            response = {
                "msg":"Error obteniendo usuario."
              };
            res.status(500);
          } else {
            if (body.length > 0 && body[0].logged ==true){
               response = body
             }
             else if (body.length > 0 && body[0].logged !=true){
               console.log("body[0].id - "+ body[0].id);
               response = {
                 "msg":"Usuario no logado", "id":body[0].id,"first_name":body[0].first_name, "logged":body[0].logged
               };
               console.log(response);
               //res.status(404);
             }
             else {
              response = {
                "msg":"Usuario no encontrado o no logado"
              };
             res.status(404);
           }
          }
        res.send(response);
      }
    )
  }
);

//Tratamiento de login de usuario
app.put("/mybalance/login",
    function(req,res){
      var bcrypt = require('bcrypt');
      console.log("PUT /mybalance/login");
      //console.log("email "+ req.body.email);
      //console.log("password "+ req.body.password);
      var email = req.body.email;
      var password = req.body.password;

      //var query = 'q={"email":"' + email + '", "password": "' + password +'"}';
      var query = 'q={"email":"' + email + '"}';
      console.log(query);
      var httpClient =requestJson.createClient(baseMLabURL);
      console.log("Cliente http creado para tratamiento login");

      httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        //console.log("body " + body.id);
        var response = {};
        //console.log("bodypwd - " + body[0].password);
         //var testPwd = bcrypt.compareSync(password, body[0].password);
         //console.log("testPWd - " + testPwd);
        if (err){
          response = {
              "msg":"Error obteniendo usuario."
            };
            console.log("Error obteniendo usuario");
          res.status(500);
        } else {
            var bcrypt = require('bcrypt');
            if (body.length > 0 && bcrypt.compareSync(password, body[0].password) ==true){
              var putBody = '{"$set":{"logged":true}}';
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody));
              response = {"msg":"login correcto" , "id_insertado" : body[0].id };
              //response = {"msg":"login correcto" , "id" : body[0].id };
            } else {
              response = {
                "msg":"Usuario no encontrado con esta contraseña"
              };
              console.log("Usuario no encontrado con esta contraseña");
              res.status(404);
            }
          }
        res.send(response);
      }
      )
  }
);

//gestión de logout de usuario
app.put("/mybalance/logout",
    function(req,res){
      console.log("PUT /mybalance/logout");
      var id = req.body.id;

      //console.log("req - " + req);
      //console.log("req.body[0] - " + req.body[0]);
      //console.log("req.body - " + req.body);
      //console.log("req.body[id] - " + req.body[id]);
      //console.log("id - " + id);

      var query = 'q={"id":' + id + '}';
      var httpClient =requestJson.createClient(baseMLabURL);
      console.log("Cliente http creado para tratamiento de logout");

      httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};
        if (err){
          response = {
              "msg":"Error obteniendo usuario."
            };
          res.status(500);
        } else {
            if (body.length > 0){
              console.log("body.length - " + body.length);
              var putBody = '{"$unset":{"logged":""}}';
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody));
              response = {"msg":"logout correcto" , "id" : body[0].id };

            } else {
              response = {
                "msg":"Usuario no encontrado"
              };
              //console.log("body.id - " + body.id);
              res.status(404);
            }
          }
        res.send(response);
      }
    )
  }
);

//gestión de logout de un usuario, devolviendo el id del usuario deslogado
app.put("/mybalance/logout/:id",
    function(req,res){
      console.log("PUT /mybalance/logout/:id");
      var id = req.params.id;

      //console.log("req - " + req);
      //console.log("req.body[0] - " + req.body[0]);
      //console.log("req.body - " + req.body);
      //console.log("req.body[id] - " + req.body[id]);
      //console.log("id - " + id);

      var query = 'q={"id":' + id + '}';
      var httpClient =requestJson.createClient(baseMLabURL);
      console.log("Cliente http creado para tratamiento de logout");

      httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};
        if (err){
          response = {
              "msg":"Error obteniendo usuario."
            };
          res.status(500);
        } else {
            if (body.length > 0){
              console.log("body.length - " + body.length);
              var putBody = '{"$unset":{"logged":""}}';
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody));
              response = {"msg":"logout correcto" , "id" : body[0].id };
              console.log(response);

            } else {
              response = {
                "msg":"Usuario no encontrado"
              };
              //console.log("body.id - " + body.id);
              res.status(404);
            }
          }
        res.send(response);
      }
    )
  }
);



  //Tratamiento de registro de un usuario que no existe
  app.post("/mybalance/users",
      function(req,res){

        var query_id = 'f={"id":1,"_id":0}&s={"id":-1}&l=1';
        //sumando 1 al id del usuario
        var httpClient =requestJson.createClient(baseMLabURL);
        console.log("Cliente http creado para sumar 1 al ID de usuario");

        httpClient.get("user?" + query_id + "&" + mLabAPIKey,
        function(err, resMLab, body){
          var response = {};
          if (err){
            response = {
                "msg":"Error obteniendo el mayor id de usuario."
              };
            res.status(500);
          } else {
              if (body.length > 0){
                newId = body[0].id +1;
                // Load the bcrypt module
                var bcrypt = require('bcrypt');
                var salt = bcrypt.genSaltSync(10);
                // Hash the password with the salt
                var hashedPwd = bcrypt.hashSync(req.body.password, salt);
                //hashedPwd = req.body.password.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);

                var full_address = req.body.address + ", " + req.body.zip_code + " " + req.body.city + ", " + req.body.country;
                console.log(full_address);
                console.log("POST /mybalance/users");
                var newSignup = {
                  "id" : newId,
                  "first_name" : req.body.first_name,
                  "last_name" : req.body.last_name,
                  "address" : req.body.address,
                  "zip_code" : req.body.zip_code,
                  "city" : req.body.city,
                  "country" : req.body.country,
                  "full_address" : full_address,
                  "email" : req.body.email,
                  "password" : hashedPwd,
                  "logged": true
                }
                var newSignupString = JSON.stringify(newSignup);
                console.log("signup - "+ newSignupString);

                var email = req.body.email;
                //var password = req.body.password;

                //var query = 'q={"email":"' + email + '", "password": "' + password +'"}';
                var query = 'q={"email":"' + email + '"}';
                var httpClient =requestJson.createClient(baseMLabURL);
                console.log("Cliente http creado para tratamiento signup");

                httpClient.get("user?" + query + "&" + mLabAPIKey,
                  function(err, resMLab, body){
                      if (err){
                        response = {
                            "msg":"Error comprobando usuario."
                        };
                        res.status(500);
                    } else {
                        if (body.length <= 0){
                            httpClient.post("user?" + mLabAPIKey, newSignup);
                            response = {"msg":"Procediendo al registro. User added successfully","id": newId };
                        } else {
                            response = {
                              "msg":"Usuario ya dado de alta con este correo electrónico."
                            };
                            res.status(404);
                          }//else
                        }//else
                      res.send(response);
                     }//function
                  )//httpCliente
              }
            }
          }
      );//app.get last user_id

       }//function
    );//app.post


//obtención de cuentas de un usuario
app.get("/mybalance/users/:id/accounts",
    function(req,res){
      console.log("GET /mybalance/users/:id/accounts");
      var id = req.params.id;
      //var query = 'q={"id":' + id + '}';
      var query = 'q={"userid":'+id+'}&f={"userid":1,"iban":1,"balance":1,"accounttype":1,"ingresos":1,"gastos":1,"_id":0}';

      var httpClient =requestJson.createClient(baseMLabURL);

      console.log("Cliente http creado para obtención de cuentas por usuario");

      httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body){
          var response = {};
          if (err){
            response = {
                "msg":"Error obteniendo cuentas."
              };
            res.status(500);
          } else {
            if (body.length > 0 ){
               response = body
             } else {
              response = {
                "msg":"No se han encontrado cuentas para este usuario"
              };
             res.status(404);
           }
          }
        res.send(response);
      }
    )
  }
);

//obtención de movimientos de una cuenta de usuario
app.get("/mybalance/users/:id/accounts/:iban/movements",
    function(req,res){
      console.log("GET /mybalance/users/:id/accounts/:iban/movements");
      var id = req.params.id;
      var iban = req.params.iban;
      var query = 'q={"userid":' + id + ',"iban":"' + iban + '"}&s={"date":-1}';
      //var query = 'q={"userid":'+id+'}&f={"userid":1,"iban":1,"balance":1,"_id":0}';
      //q={"user_id":31,"iban":"IE37BOMS67391785601372"}&s={"date":-1}
      console.log(query);

      var httpClient =requestJson.createClient(baseMLabURL);

      console.log("Cliente http creado para obtención de movimientos de una cuenta de un usuario");

      httpClient.get("movement?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body){
          var response = {};
          console.log(body);
          if (err){
            response = {
                "msg":"Error obteniendo movimientos de la cuenta."
              };
            res.status(500);
          } else {
            if (body.length > 0){
               response = body
             } else {
              response = {
                "msg":"No se han encontrado movimientos para esta cuenta"
              };
             res.status(404);
           }
          }
        res.send(response);
      }
    )
  }
);
/// tratamiento/edición de movimientos ingresos o gastos
app.put("/mybalance/users/:id/accounts/:iban/movements",
    function(req,res){
      console.log("PUT /mybalance/users/:id/accounts/:iban/movements");
      var id = req.params.id;
      var iban = req.params.iban;
      var amount = req.body.amount;
      amount = Number(amount);
      var movement_desc = req.body.movement_desc;
      var movement_category = req.body.movement_category;
      var movement_currency = req.body.movement_currency;
      var day=new Date().toISOString().slice(0,10);
      var hour = new Date().toISOString().slice(11,19);
      var date_movement=day+" "+hour;
      console.log("date_movement " + date_movement);
      if (movement_category == "Reintegro"){
        amount = amount * -1
      }
      console.log("IMPRIMIR" + amount);

      var query = 'q={"userid":' + id + ',"iban":"'+iban+'"}&f={"movements":1,"_id":0}';
      //q={"userid":29,"iban":"IE37BOMS67391785555572"}&f={"movements":1,"_id":0}
      console.log("Query - " + query);

      httpClient =requestJson.createClient(baseMLabURL);
      console.log("Cliente http creado para introducción de movimientos para una cuenta");

      httpClient.get("movement?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};
        console.log("body[] - " +body[0]);
        if (err){
          response = {
              "msg":"Error obteniendo movimientos para una cuenta del usuario."
            };
          res.status(500);
        } else {
            if (body.length >= 0){
              //console.log("body.length - " + body.length);
              //console.log("body - " + body);
              //var date = Date("<YYYY-mm-dd>"); //new Date().toISOString();



              var putBody = '{"$push":{"movements":{"movement_desc":"'+movement_desc+'","movement_category":"'+movement_category+'","amount":'+amount+',"movement_currency":"'+movement_currency+'", "movement_date":"' +date_movement+'"}}}';
              httpClient.put("movement?" + query + "&" + mLabAPIKey, JSON.parse(putBody));

              //response = {"msg":"inserción movimiento correcta - pendiente de consolidar" , "amount" : amount, "userid":userid, "iban": iban };
              response = {"msg":"inserción movimiento correcta - pendiente de consolidar" , "amount" : amount, "id":Number(id),"iban":iban};
              var query_get_balance = 'q={"userid":' + id + ',"iban":"'+iban+'"}&f={"balance":1,"_id":0}';

              httpClient.get("account?" + query_get_balance + "&" + mLabAPIKey,
                function(err, resMLab, body){
                  if(!err){
                    var newBalance = Number(body[0].balance) + amount;
                    var putBody_newBalance = '{"$set":{"balance":'+newBalance+'}}';
                    //console.log(body[0].balance);
                    //console.log(amount);
                    //console.log(newBalance);
                    httpClient.put("account?" + query_get_balance + "&" + mLabAPIKey, JSON.parse(putBody_newBalance));
                    //response = {"msg":"Saldo actualizado" ,  "newBalance" : newBalance }; //CARLOS - no se muestra este Response
                    response = {"msg":"Saldo actualizado" , "id":id, "iban": iban}; //CARLOS - no se muestra este Response
                    console.log(response);
                    //res.send(response);


                  }
                }
              );
            } else {
              response = {
                "msg":"Error obteniendo movimientos para una cuenta del usuario."
              };
              //console.log("body.id - " + body.id);
              res.status(404);
            }
          }
        res.send(response);
      }//function


    );
  }
);

//tratamiento de traspaso o transferencia
app.put("/mybalance/users/:id/accounts/:iban/movements_t",
    function(req,res){
      console.log("PUT /mybalance/users/:id/accounts/:iban/movements_t");
      var id = req.params.id;
      var iban = req.params.iban;
      var amount = req.body.amount;
      amount = Number(amount) * -1;
      var movement_desc = req.body.movement_desc;
      var movement_category = req.body.movement_category;
      var iban_dest = req.body.iban_dest;
      var day=new Date().toISOString().slice(0,10);
      var hour = new Date().toISOString().slice(11,19);
      var date_movement=day+" "+hour;
      console.log("date_movement " + date_movement);

      console.log("IMPRIMIR" + amount);

      var query = 'q={"userid":' + id + ',"iban":"'+iban+'"}&f={"movements":1,"_id":0}';
      //q={"userid":29,"iban":"IE37BOMS67391785555572"}&f={"movements":1,"_id":0}
      console.log("Query - " + query);

      httpClient =requestJson.createClient(baseMLabURL);
      console.log("Cliente http creado para introducción de movimientos para una cuenta");

      httpClient.get("movement?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};
        console.log("body[] - " +body.length);
        if (body.length >= 0){
              //console.log("body.length - " + body.length);
              //console.log("body - " + body);
              //var date = Date("<YYYY-mm-dd>"); //new Date().toISOString();
              console.log("body.length es >= 0 " + body.length);
              console.log("query pushed - " + query);
              var putBody = '{"$push":{"movements":{"movement_desc":"'+movement_desc+'","movement_category":"'+movement_category+'","amount":'+amount+', "movement_date":"' +date_movement+'"}}}';
              console.log("putBody - " + putBody);
              httpClient =requestJson.createClient(baseMLabURL);
              console.log("Cliente http creado para introducción de movimientos para una cuenta");
              httpClient.put("movement?" + query + "&" + mLabAPIKey, JSON.parse(putBody));

              //response = {"msg":"inserción movimiento correcta - pendiente de consolidar" , "amount" : amount, "userid":userid, "iban": iban };
              response = {"msg":"inserción movimiento correcta - pendiente de consolidar" , "amount" : amount, "id":Number(id),"iban":iban};
              var query_get_balance = 'q={"userid":' + id + ',"iban":"'+iban+'"}&f={"balance":1,"_id":0}';

              httpClient.get("account?" + query_get_balance + "&" + mLabAPIKey,
                function(err, resMLab, body){
                  if(!err){
                    var newBalance = Number(body[0].balance) + amount;
                    var putBody_newBalance = '{"$set":{"balance":'+newBalance+'}}';
                    console.log(body[0].balance);
                    console.log(amount);
                    console.log(newBalance);
                    httpClient.put("account?" + query_get_balance + "&" + mLabAPIKey, JSON.parse(putBody_newBalance));
                    //response = {"msg":"Saldo actualizado" ,  "newBalance" : newBalance }; //CARLOS - no se muestra este Response
                    response = {"msg":"Saldo actualizado" , "id":id, "iban": iban}; //CARLOS - no se muestra este Response
                    console.log(response);
                    //res.send(response);
                  }
                }
              );

              var query_dest ='q={"iban":"'+iban_dest+'"}&f={"movements":1,"_id":0}';
              console.log("query_dest - " + query_dest);
              console.log("date_movement -" +date_movement);
              httpClient =requestJson.createClient(baseMLabURL);
              console.log("Cliente http creado para obtención de movimientos del IBAN destino");
              httpClient.get("movement?" + query_dest + "&" + mLabAPIKey,
                function(err, resMLab, body){
                  if (!err){
                    var putBody_dest = '{"$push":{"movements":{"movement_desc":"'+movement_desc+'","movement_category":"'+movement_category+'","amount":'+amount*-1+',"movement_date":"' +date_movement+'"}}}';
                    console.log("putBody_dest - " +putBody_dest);
                    httpClient.put("movement?" + query_dest + "&" + mLabAPIKey, JSON.parse(putBody_dest));
                    console.log("put - " + "movement?" + query_dest);

                    var query_get_balance_dest = 'q={"iban":"'+iban_dest+'"}&f={"balance":1,"_id":0}';

                    httpClient =requestJson.createClient(baseMLabURL);
                    console.log("Cliente http creado para obtención del balance del IBAN destino");

                    httpClient.get("account?" + query_get_balance_dest + "&" + mLabAPIKey,
                      function(err, resMLab, body){
                        if(!err){
                            var newBalance_dest = Number(body[0].balance) + amount*-1;
                            var putBody_newBalance_dest = '{"$set":{"balance":'+newBalance_dest+'}}';

                            httpClient.put("account?" + query_get_balance_dest + "&" + mLabAPIKey, JSON.parse(putBody_newBalance_dest));
                            //response = {"msg":"Saldo actualizado" ,  "newBalance" : newBalance }; //CARLOS - no se muestra este Response
                            response = {"msg":"Saldo actualizado" ,  "iban_dest": iban_dest}; //CARLOS - no se muestra este Response
                            console.log(response);
                                //res.send(response);
                        }//if no error
                      } //function
                    );//get account

                  }
                  else {
                    console.log("Cuenta sin movimientos");
                  }

                }//function get movement iban_dest
              );//get movement iban_dest

            } //if body.length >= 0
            else {
              response = {
                "msg":"Error obteniendo movimientos para una cuenta del usuario."
              };
              //console.log("body.id - " + body.id);
              res.status(404);
            }

        res.send(response);

      }//function get movement
    );//get movement


  } //function put movements_t
);//put movements_t


//creación de una cuenta aleatoria (de tipo ahorro)
app.post('/mybalance/users/:id/accounts',
 function(req, res) {
   console.log("POST /mybalance/users/:id/accounts");

   var day=new Date().toISOString().slice(0,10);
   var hour = new Date().toISOString().slice(11,19);
   var date_movement=day+" "+hour;

    httpClient =requestJson.createClient(baseMLabURL);
    console.log("Cliente http creado para creación de una cuenta aleatoria");

   //generamos cta aleatoriamante
    var iban = "ES34";
    var ccc="";

    for (i = 0; i < 5; i++) {
      ccc=Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 4);
      iban=iban+" "+ccc;
     }
     //console.log(iban);
     var accounttype = "Ahorro";

       var postAccount = '{"userid" :'+req.params.id+',"iban" : "' + iban.toUpperCase() + '", "balance": 0.00 , "accounttype": "'+accounttype+'"}';
           httpClient.post("account?" + mLabAPIKey, JSON.parse(postAccount),
             function(err, resMLab, body){
               console.log("body" +body[0]);
               if (err) {
                 response = {"msg" : "Error insertando cuenta por defecto para usuario."}
                 //res.send(response);
                 res.status(500);
               } else {
               var response = {"msg" : "Nueva cuenta dada de alta con éxito", "cta": iban.toUpperCase(), "userid" :req.params.id}
               //res.send(response);

                //INSERTAR PRIMER MOVIMIENTO EN TABLA MOVEMENT.
                var postMovement ='{"userid" :'+req.params.id+',"iban" : "' + iban.toUpperCase() +'", "movements": [{"movement_desc":"Movimiento de Apertura","movement_category": "Apertura","amount": 0.00, "movement_date":"'+date_movement+'" }]}';
                console.log("postMovement - " +postMovement);
                httpClient.post("movement?" + mLabAPIKey, JSON.parse(postMovement),
                  function(err, resMLab, body){
                }
              );

             }
                res.send(response);
           }//fin function  post
           );
        }
      );

//edita info usuario
app.put('/mybalance/users/:id',
    function(req, res) {
    console.log("PUT /mybalance/users/:id");

    var id = req.params.id;
    var email = req.body.email;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    var address = req.body.address;
    var zip_code = req.body.zip_code;
    var city = req.body.city;
    var country = req.body.country;
    var full_address = req.body.address + ", " + req.body.zip_code + " " + req.body.city + ", " + req.body.country;


    var putQuery = 'q={"id" : ' + id + '}';
    httpClient =requestJson.createClient(baseMLabURL);
    console.log("Cliente http creado para edición de datos de un usuario");
    var putBody = '{"$set":{"email" : "' + email + '","first_name": "' + first_name + '","last_name": "' + last_name + '" ,"address": "' + address +'","zip_code": "' + zip_code +'","city": "' + city +'", "country":"'+country+'","full_address":"'+full_address+'"}}';
    console.log("putBody - " +putBody);

    if(email.length>0&first_name.length>0&last_name.length>0&address.length>0&zip_code.length>0&city.length>0&country.length>0){
        httpClient.put("user?" + putQuery + "&" + mLabAPIKey, JSON.parse(putBody),
                 function(err, resMLab, body) {
                   }//fin del put
                 );//fin httpClient del put
                 var response = {
                     "msg" : "Información actualizada"//, "id": body[0].id
                 }

                //res.send(response);
             }

             else{
               var response = {
                   "msg" : "Faltan campos"
               }
                  //res.send(response);
             }
             res.send(response);
});


//edita pwd usuario
app.put('/mybalance/users/:id/pwd',
          function(req, res) {
          console.log("PUT /mybalance/users/:id/pwd");

          var id = req.params.id;
          var password = req.body.password;
          var bcrypt = require('bcrypt');
          var salt = bcrypt.genSaltSync(10);
          // Hash the password with the salt
          var hashedPwd = bcrypt.hashSync(password, salt);


          var putQuery = 'q={"id" : ' + id + '}';
          httpClient =requestJson.createClient(baseMLabURL);
          console.log("Cliente http creado para edición de la contraseña de un usuario");
          var putBody = '{"$set":{"password" : "' + hashedPwd + '"}}';
          console.log("putBody - " +putBody);

          if(password.length>0){
              httpClient.put("user?" + putQuery + "&" + mLabAPIKey, JSON.parse(putBody),
                       function(err, resMLab, body) {
                         }//fin del put
                       );//fin httpClient del put
                       var response = {
                           "msg" : "Información actualizada"//, "id": body[0].id
                       }

                      //res.send(response);
                   }

                   else{
                     var response = {
                         "msg" : "Faltan campos"
                     }
                        //res.send(response);
                   }
                   res.send(response);
            });

//agregación de movimientos en Ingresos y Gastos
app.put('/mybalance/users/:id/accounts/:iban/aggregate',
    function(req, res) {
      console.log("PUT /mybalance/users/:id/accounts/:iban/aggregate");
      var id = req.params.id;
      var iban = req.params.iban;
      //var movement_category = req.body.movement_category;

      var query_mov = 'q={"userid":' + id + ',"iban":"'+iban+'"}&f={"movements":1,"_id":0}';
      var query_acc = 'q={"userid":' + id + ',"iban":"'+iban+'"}';
      console.log("Query Acc - " + query_acc);

      httpClient =requestJson.createClient(baseMLabURL);
      console.log("Cliente http creado para agregación de movimientos para una cuenta");

      httpClient.get("movement?" + query_mov + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};
        console.log("body[] - " +body[0]);
        if (err){
          response = {
              "msg":"Error obteniendo movimientos para una cuenta del usuario."
            };
          res.status(500);
        } else {
            if (body.length > 0){
              var aggregation_ingreso = 0;
              var aggregation_gasto = 0;

              for (i = 0; i < body[0].movements.length; i++) {
                if(body[0].movements[i].movement_category == "Ingreso") {
                aggregation_ingreso = Number(aggregation_ingreso) + Number(body[0].movements[i].amount);
                }
                else if(body[0].movements[i].movement_category == "Reintegro" || body[0].movements[i].movement_category =="Transferencia"){
                aggregation_gasto = Number(aggregation_gasto) + Number(body[0].movements[i].amount);
                }
              }


              httpClient.get("account?" + query_acc + "&" + mLabAPIKey,
                function(err, resMLab, body){
                  if(!err){
                    console.log("body[0].userid " + body[0].userid);
                    var putBody = '{"$set":{"ingresos":'+aggregation_ingreso+', "gastos":'+aggregation_gasto+'}}';
                    console.log("putBody " + putBody);
                    console.log("query_acc " + query_acc);
                    httpClient.put("account?" + query_acc + "&" + mLabAPIKey, JSON.parse(putBody));
                    }
                  }
                );
        }

      }
    }
  );
  }
);
